let arr = ['hello', 'world', 23, '23', null, {num: 1, num3: 3}];

function filterBy (array, theType) {
    let newArr = [];
    for (let i = 0; i < array.length; i++ )  {
        if (typeof array[i] !== theType || Boolean(array[i]) === false) {
            newArr.push(array[i])
        }
    }
return newArr;
}


console.log( filterBy(arr, 'string'));